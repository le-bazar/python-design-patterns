"""Implementation of the facade pattern.

With this pattern we are using only one object that made the call to multiple
objects for us. The complexity is hidden for the user.
"""
class SystemA:
    def __init__(self) -> None:
        self._count = 0

    @property
    def count(self) -> int:
        return self._count

    def operation_one(self) -> None:
        self._count += 1
        print('Operation one of system A')

    def operation_two(self) -> None:
        self._count += 2
        print('Operation two of system A')

    def reset(self) -> None:
        self._count = 0


class SystemB:
    def __init__(self) -> None:
        self._count = 0

    @property
    def count(self) -> int:
        return self._count

    def operation_one(self) -> None:
        self._count += 1
        print('Operation one of system B')

    def operation_two(self) -> None:
        self._count += 2
        print('Operation two of system B')

    def reset(self) -> None:
        self._count = 0


class SystemC:
    def __init__(self) -> None:
        self._count = 0

    @property
    def count(self) -> int:
        return self._count

    def operation_one(self) -> None:
        self._count += 1
        print('Operation one of system C')

    def reset(self) -> None:
        self._count = 0


class SystemNotAvailable(Exception):
    pass


class Facade:
    def __init__(self) -> None:
        self.sa = SystemA()
        self.sb = SystemB()
        self.sc = SystemC()

    def operation_one(self) -> None:
        self.sa.operation_one()
        self.sb.operation_one()
        self.sc.operation_one()

    def operation_two(self) -> None:
        self.sa.operation_two()
        self.sb.operation_two()

    def reset(self) -> None:
        self.sa.reset()
        self.sb.reset()
        self.sc.reset()

    def count(self, system: str) -> int:
        if system in ['a', 'A']:
            return self.sa.count
        elif system in ['b', 'B']:
            return self.sb.count
        elif system in ['c', 'C']:
            return self.sc.count
        else:
            raise SystemNotAvailable(f'System {system} not available')

if __name__ == "__main__":
    facade = Facade()
    facade.operation_one()
    print('#' * 20)
    facade.operation_two()
    print('#' * 20)
    print(facade.count('a'))
    print(facade.count('b'))
    print(facade.count('c'))
    print('#' * 20)
    print(facade.count('A'))
    print(facade.count('B'))
    print(facade.count('C'))
    print('#' * 20)
    print('RESET')
    facade.reset()
    print('#' * 20)
    print(facade.count('A'))
    print(facade.count('B'))
    print(facade.count('C'))
    print('#' * 20)
    print(facade.count('d'))
