from facade.facade import Facade

OUT_ONE = 'Operation one of system A\nOperation one of system B\nOperation one of system C\n'  # noqa
OUT_TWO = 'Operation two of system A\nOperation two of system B\n'
OUT_ALL = 'Operation one of system A\nOperation one of system B\nOperation one of system C\nOperation two of system A\nOperation two of system B\n'  # noqa


class TestFacade:
    def test_operation_one(self, capfd):
        facade = Facade()
        facade.operation_one()
        out, err = capfd.readouterr()
        assert out == OUT_ONE
        assert facade.count('a') == 1
        assert facade.count('b') == 1
        assert facade.count('c') == 1

    def test_operation_two(self, capfd):
        facade = Facade()
        facade.operation_two()
        out, err = capfd.readouterr()
        assert out == OUT_TWO
        assert facade.count('a') == 2
        assert facade.count('b') == 2
        assert facade.count('c') == 0

    def test_operations(self, capfd):
        facade = Facade()
        facade.operation_one()
        facade.operation_two()
        out, err = capfd.readouterr()
        assert out == OUT_ALL
        assert facade.count('a') == 3
        assert facade.count('b') == 3
        assert facade.count('c') == 1

    def test_reset(self, capfd):
        facade = Facade()
        facade.operation_one()
        facade.reset()
        out, err = capfd.readouterr()
        assert out == OUT_ONE
        assert facade.count('a') == 0
        assert facade.count('b') == 0
        assert facade.count('c') == 0