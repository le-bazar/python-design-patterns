"""Implementation of the factory pattern.

In this example the factory class :class:`~factory.factory.OperationFactory` create
instance of the concrete operation class depending of the type received as 
parameter.
The return type of this function is the abstract class. 
"""
from abc import ABC, abstractmethod
from enum import auto, Enum
from typing import List, Mapping, Type, Union


# #############################################################################
# ################################# ENUM ######################################
# #############################################################################
class AutoName(Enum):
    def _generate_next_value_(name, start, count, last_values):  # noqa
        return name


class OperationEnum(AutoName):
    ADDITION = auto()
    SUBSTRACTION = auto()
    MULTIPLICATION = auto()
    DIVISION = auto()
# #############################################################################


# #############################################################################
# ############################### OPERATION ###################################
# #############################################################################
class Operation(ABC):
    _name: str = 'Operation'

    @property
    def name(self) -> str:
        return self._name

    @abstractmethod
    def compute(self, x: int, y: int) -> Union[int, float]:
        pass


class Addition(Operation):
    _name: str = 'Addition'

    def compute(self, x: int, y: int) -> int:
        return x + y


class Substraction(Operation):
    _name: str = 'Substraction'

    def compute(self, x: int, y: int) -> int:
        return x - y


class Multiplication(Operation):
    _name: str = 'Multiplication'

    def compute(self, x: int, y: int) -> int:
        return x * y


class Division(Operation):
    _name: str = 'Division'

    def compute(self, x: int, y: int) -> float:
        return x / y
# #############################################################################


# #############################################################################
# ################################ FACTORY ####################################
# #############################################################################
class OperationFactory:
    _operations: Mapping[str, Type[Operation]] = {
        'ADDITION': Addition,
        'SUBSTRACTION': Substraction,
        'MULTIPLICATION': Multiplication,
        'DIVISION': Division
    }

    @classmethod
    def get_operation(cls, operation_type: OperationEnum) -> Operation:
        return cls._operations[operation_type.value]()


if __name__ == '__main__':
    x = 4
    y = 2

    op = OperationFactory.get_operation(OperationEnum.ADDITION)
    print(op.compute(x, y))

    op = OperationFactory.get_operation(OperationEnum.SUBSTRACTION)
    print(op.compute(x, y))

    op = OperationFactory.get_operation(OperationEnum.MULTIPLICATION)
    print(op.compute(x, y))

    op = OperationFactory.get_operation(OperationEnum.DIVISION)
    print(op.compute(x, y))
