"""Implemetation of the factory patern as shown in wikipedia.

The difference with the *more traditional* example in the factory module is that
in this example it's not one class that create multiple concrete class based on
a parameter.
It's inside a ABC class that we define the factory method and it's
in it's child class that we implement the creation of the concrete class.
"""
from abc import ABC, abstractmethod
from typing import List


# #############################################################################
# ################################# ROOM ######################################
# #############################################################################
class Room(ABC):
    def __init__(self) -> None:
        self._connected_rooms: List['Room'] = []

    def connect(self, other_room: 'Room') -> None:
        self._connected_rooms.append(other_room)


class CommonRoom(Room):
    def __str__(self) -> str:
        return 'Common Room'


class MagicRoom(Room):
    def __str__(self) -> str:
        return 'Magic Room'
# #############################################################################


# #############################################################################
# ################################# MAZE ######################################
# #############################################################################
class MazeGame(ABC):
    def __init__(self, size: int) -> None:
        self._size = size
        self._rooms: List[Room] = []
        self._prepare_room()

    def _prepare_room(self) -> None:
        last_room = None
        for i in range(self._size):
            room = self._make_room()
            if last_room:
                last_room.connect(room)
            self._rooms.append(room)

    def play(self) -> None:
        print(f'Play with {self._rooms[0]}')

    @abstractmethod
    def _make_room(self) -> Room:
        pass


class CommonMaze(MazeGame):
    def _make_room(self) -> Room:
        return CommonRoom()


class MagicMaze(MazeGame):
    def _make_room(self) -> Room:
        return MagicRoom()


if __name__ == "__main__":
    common_maze = CommonMaze(10)
    common_maze.play()

    magic_maze = MagicMaze(10)
    magic_maze.play()