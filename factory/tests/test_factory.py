from factory.factory import OperationEnum, OperationFactory
from factory.maze import CommonMaze, MagicMaze


class TestFactory:
    def test_operation_factory_addition(self):
        x = 4
        y = 2
        op = OperationFactory.get_operation(OperationEnum.ADDITION)
        assert op.compute(x, y) == op.compute(y, x) == 6

    def test_operation_factory_substraction(self):
        x = 4
        y = 2
        op = OperationFactory.get_operation(OperationEnum.SUBSTRACTION)
        assert op.compute(x, y) == 2

    def test_operation_factory_multiplication(self):
        x = 4
        y = 2
        op = OperationFactory.get_operation(OperationEnum.MULTIPLICATION)
        assert op.compute(x, y) == op.compute(y, x) == 8

    def test_operation_factory_division(self):
        x = 4
        y = 2
        op = OperationFactory.get_operation(OperationEnum.DIVISION)
        assert op.compute(x, y) == 2


class TestMaze:
    def test_common_maze(self, capfd):
        cm = CommonMaze(1)
        cm.play()
        out, err = capfd.readouterr()
        assert out == 'Play with Common Room\n'

    def test_magic_maze(self, capfd):
        mm = MagicMaze(1)
        mm.play()
        out, err = capfd.readouterr()
        assert out == 'Play with Magic Room\n'