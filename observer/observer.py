"""Implemetation of the observer pattern.

In this implementation we got some observers class with a notify method and a
subject class that was the main class. We a subject object you can register
observers.

When the run method of the subject is called the registered
observers are notified.
"""
from abc import ABC, abstractmethod
from random import randint
from typing import Any, Dict, List, Optional


# #############################################################################
# ############################### OBSERVER ####################################
# #############################################################################
class BaseObserver(ABC):
    """Base class for the observers."""

    @abstractmethod
    def notify(self, data: Optional[Dict[str, Any]]) -> None:
        pass


class PrintObserver(BaseObserver):
    """Observer that print the data received.
    
    When the notify method is called, the observer just print on the standard
    output the data received.
    """

    def notify(self, data: Optional[Dict[str, Any]]) -> None:
        print(f'Observer notified with data : {data}')


class LogObserver(BaseObserver):
    """Observer that should log the data received.
    
    When the notify method is called, the observer should log the data received
    but, because this code is for testing purpose, the method print to the
    standard output.
    """
    
    def notify(self, data: Optional[Dict[str, Any]]) -> None:
        # Here I should log
        print(f'Should log with data : {data}')


# #############################################################################
# ################################ SUBJECT ####################################
# #############################################################################
class Subject:
    def __init__(self) -> None:
        self._observers: List[BaseObserver] = []

    def register_observer(self, observer: BaseObserver) -> None:
        """Register a new observer."""
        self._observers.append(observer)

    def notify_observers(self, data: Optional[Dict[str, int]] = None) -> None:
        """Call the notify method for each of the registered observers."""
        for obs in self._observers:
            obs.notify(data)

    def clear_observers(self) -> None:
        """Clear the list of observers."""
        self._observers = []

    def run(self) -> None:
        """Generate random data and notify the registered observers."""
        number = randint(0, 1000)
        self.notify_observers({'number': number})


if __name__ == '__main__':
    subject = Subject()

    subject.register_observer(PrintObserver())
    subject.register_observer(LogObserver())

    subject.run()

    subject.clear_observers()

    subject.run()
