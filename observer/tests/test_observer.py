import random

from observer.observer import Subject, PrintObserver, LogObserver

LOG_MSG = "Should log with data : {'number': 137}\n"
PRINT_MSG = "Observer notified with data : {'number': 137}\n"


class TestObserver:
    def test_print_observer(self, capfd):
        s = Subject()
        s.register_observer(PrintObserver())
        s.run()
        out, err = capfd.readouterr()
        assert out == PRINT_MSG

    def test_log_observer(self, capfd):
        s = Subject()
        s.register_observer(LogObserver())
        s.run()
        out, err = capfd.readouterr()
        assert out == LOG_MSG

    def test_print_n_log_observer(self, capfd):
        s = Subject()
        s.register_observer(PrintObserver())
        s.register_observer(LogObserver())
        s.run()
        out, err = capfd.readouterr()
        assert out == f'{PRINT_MSG}{LOG_MSG}'
