"""Use the inner class system to create a singleton.

Pros:

- Easy implementation

Cons:

- The outer class is not a singleton, a new object is created
- You have to duplicate the implementation for each class you want to be a singleton
"""
from typing import Any, Optional


class MySingleton:
    class __RealClass:
        def __init__(self) -> None:
            self._data: Optional[str] = None

        @property
        def data(self) -> Optional[str]:
            return self._data

        @data.setter
        def data(self, value: str) -> None:
            self._data = value

    _instance = None
    def __init__(self) -> None:
        if MySingleton._instance is None:
            MySingleton._instance = MySingleton.__RealClass()

    def __getattr__(self, name: str):
        return getattr(self._instance, name)

    def __setattr__(self, name: str, value: Any):
        return setattr(self._instance, name, value)

if __name__ == '__main__':
    t_01 = MySingleton()
    t_02 = MySingleton()
    print(id(t_01) != id(t_02))
    print(id(t_01._instance) == id(t_02._instance))
