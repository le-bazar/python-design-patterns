"""Use a metaclass to create a singleton.

Pros:

- Works for multiple class

Cons:

- Not as easy as the inner class
"""
from typing import Type


class SingletonMeta(type):
    __instance = None 

    def __call__(cls, *args, **kwargs) -> Type:
        print('You should use the "get_instance" method')
        return cls.get_instance(*args, **kwargs)

    def get_instance(cls, *args, **kwargs) -> Type:
        """Method that create or return the already created instance.
        
        In python the constructor cannot be private, this method is here just to
        allow the code to looks similar on what can be found in other language.
        """
        if cls.__instance is None:
            # Here where the __init__ of the child class is called 
            cls.__instance = super().__call__(*args, **kwargs)
        return cls.__instance


if __name__ == '__main__':
    class Test(metaclass=SingletonMeta):
        def __init__(self):
            self._data = None

        @property
        def data(self) -> str:
            return self._data

        @data.setter
        def data(self, value: str) -> None:
            self._data = value

    class Test2(metaclass=SingletonMeta):
        def __init__(self):
            self._data = None
            self._state = 1

        @property
        def data(self) -> str:
            return self._data

        @data.setter
        def data(self, value: str) -> None:
            self._data = value

        @property
        def state(self) -> int:
            return self._state

        def next(self) -> None:
            self._state += 1

        def prev(self) -> None:
            self._state -= 1

    t_01 = Test()
    t_02 = Test()
    t_03 = Test.get_instance()
    assert isinstance(t_01, Test)
    assert isinstance(t_02, Test)
    assert isinstance(t_03, Test)
    print(id(t_01) == id(t_02) == id(t_03))

    t_04 = Test2.get_instance()
    t_05 = Test2()
    t_06 = Test2()
    assert isinstance(t_04, Test2)
    assert isinstance(t_05, Test2)
    assert isinstance(t_06, Test2)
    print(id(t_04) == id(t_05) == id(t_06))
    t_04.next()
    assert t_04.state == t_05.state == t_06.state
