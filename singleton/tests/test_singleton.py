from singleton.metaclass import SingletonMeta
from singleton.inner_class import MySingleton


class TestSingleton:
    def test_singleton_meta(self):
        """Test the signleton patern using the metaclass."""
        class C(metaclass=SingletonMeta):
            """A simple class used for testing."""
            def __init__(self):
                self._name = 'C01'

            @property
            def name(self) -> str:
                return self._name

            @name.setter
            def name(self, new_name: str) -> None:
                self._name = new_name

        c_01 = C()
        c_02 = C()
        assert id(c_01) == id(c_02)
        assert c_01.name == c_02.name
        c_01.name = 'C01_updated'
        assert c_01.name == c_02.name == 'C01_updated'

    def test_singleton_inner_class(self):
        c_01 = MySingleton()
        c_02 = MySingleton()
        assert id(c_01) != id(c_02)
        assert id(c_01._instance) == id(c_02._instance)
        c_01.data = 'new_data'
        assert c_01.data == c_02.data == 'new_data'
